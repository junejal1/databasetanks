﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MongoDB.Bson.Serialization.Attributes;
using System.Data;
using Mono.Data.Sqlite;
using Complete;
using MongoDB.Driver;

public class DatabaseManager : MonoBehaviour
{
    public static DatabaseManager databaseManager;
    [HideInInspector]public int shotsFiredAI;
    [HideInInspector]public int shotsFiredPlayer;
    [HideInInspector] public float roundStartTime = 0;
    [HideInInspector] public float roundEndTime = 0;

    public class Highscore
    {
        [BsonId] 
        public int id { get; set; }
        public string Username { get; set; }
        public float Accuracy { get; set; }
        public int TotalRoundsWon { get; set; }
    }

    Highscore highscore;
    private void Start()
    {
        databaseManager = this;
        shotsFiredAI = 0;
        shotsFiredPlayer = 0;
        ReadMongoDatabase();
    }

    public void UpdateDestroyLocation(GameObject gameObject)
    {
        float xPos = gameObject.transform.position.x;
        float zPos = gameObject.transform.position.z;
        string connectionString = "URI=file:" + Application.dataPath + "/Resources/Database/GameStats.db";
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();

            IDbCommand command = dbConnection.CreateCommand();
            command.CommandText = "INSERT INTO DestroyLocationData (Xpos, Zpos) VALUES (@Xpos, @Zpos)";
            command.Parameters.Add(new SqliteParameter("@Xpos", xPos));
            command.Parameters.Add(new SqliteParameter("@Zpos", zPos));

            command.ExecuteNonQuery();

            dbConnection.Close();
        }
    }

    public void UpdateRoundData(Complete.TankManager[] tanks, int roundNumber, Complete.TankManager roundWinner)
    {
        // get data
        float elapsedTime = roundEndTime - roundStartTime;
        float distanceAI = 0;
        float distancePlayer = 0;
        int playerShots = 0;
        int aiShots = 0;
        for (int i = 0; i < tanks.Length; i++)
        {
            if (tanks[i].m_Instance.GetComponent<TankAI>()!=null)
            {
                distanceAI = Vector3.Distance(tanks[i].m_SpawnPoint.position, tanks[i].m_Instance.transform.position);
                aiShots = tanks[i].m_AI.shotCounter;
                tanks[i].m_AI.shotCounter = 0; ;
            }
            else
            {
                distancePlayer = Vector3.Distance(tanks[i].m_SpawnPoint.position, tanks[i].m_Instance.transform.position);
                playerShots = tanks[i].returnShots();
                tanks[i].setShots(0);
            }
        }

        string connectionString = "URI=file:" + Application.dataPath + "/Resources/Database/GameStats.db";
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();

            IDbCommand command = dbConnection.CreateCommand();
            command.CommandText = "INSERT INTO Round" + roundNumber + " (RoundTime, PLayerShots, AIShots, PlayerDistance, AIDistance) " +
                                                                "VALUES (@RoundTime, @PLayerShots, @AIShots, @PlayerDistance, @AIDistance)";
            command.Parameters.Add(new SqliteParameter("@RoundTime", elapsedTime));
            command.Parameters.Add(new SqliteParameter("@PlayerShots", playerShots));
            command.Parameters.Add(new SqliteParameter("@AIShots", aiShots));
            command.Parameters.Add(new SqliteParameter("@PlayerDistance", distancePlayer));
            command.Parameters.Add(new SqliteParameter("@AIDistance", distanceAI));

            command.ExecuteNonQuery();

            dbConnection.Close();
        }
        shotsFiredAI += aiShots;
        shotsFiredPlayer += playerShots;

        if(Complete.GameManager.gameManager.GetGameWinner() != null)
            UpdateShotsFiredData(tanks);

        if (roundWinner.m_Instance.GetComponent<TankAI>() == null)
        {
            highscore.TotalRoundsWon++;
            UpdateMongoDBRoundsCount();
        }
    }

    public void UpdateShotsFiredData(Complete.TankManager[] tanks)
    {

        string connectionString = "URI=file:" + Application.dataPath + "/Resources/Database/GameStats.db";
        using (IDbConnection dbConnection = new SqliteConnection(connectionString))
        {
            dbConnection.Open();

            IDbCommand command = dbConnection.CreateCommand();
            command.CommandText = "INSERT INTO ShotsFiredPerGame (PlayerShots, AIShots) Values (@PlayerShots, @AIShots)";
            command.Parameters.Add(new SqliteParameter("@PlayerShots", shotsFiredPlayer));
            command.Parameters.Add(new SqliteParameter("@AIShots", shotsFiredAI));

            command.ExecuteNonQuery();

            dbConnection.Close();
        }
        UpdateMongoDBAccuracy(tanks);
        shotsFiredPlayer = 0;
        shotsFiredAI = 0;

    }

    void  ReadMongoDatabase()
    {
        MongoClient client = new MongoClient("mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/<dbname>?retryWrites=true&w=majority");
        var db = client.GetDatabase("GDAP_Exercise");
        var coll = db.GetCollection<Highscore>("Highscores").Find(u => u.id == 991556881);
        if(coll.CountDocuments() > 0)
        {
            highscore = coll.Single();
            Debug.Log("" + highscore.id + ", " + highscore.Username + ", " + highscore.Accuracy + ", " + highscore.TotalRoundsWon + ".");
        }
    }

    private void UpdateMongoDBAccuracy(Complete.TankManager[] tanks)
    {
        float accuracy = 0;
        float hits = 0;
        //Calculate accuracy
        for (int i = 0; i < tanks.Length; i++)
        {
            if (tanks[i].m_Instance.GetComponent<TankAI>() != null)
            {
                hits = tanks[i].m_Instance.GetComponent<Complete.TankHealth>().hitcounter;
            }
        }
        accuracy = hits / (float)shotsFiredPlayer;
        Debug.Log("accuracy " + accuracy);
        if(accuracy > highscore.Accuracy)
        {
            highscore.Accuracy = accuracy;
            MongoClient client = new MongoClient("mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/<dbname>?retryWrites=true&w=majority");
            var db = client.GetDatabase("GDAP_Exercise");
            var coll = db.GetCollection<Highscore>("Highscores");
            coll.ReplaceOne(u => u.id == highscore.id, highscore);
        }
    }

    private void UpdateMongoDBRoundsCount()
    {
        //update the db
        MongoClient client = new MongoClient("mongodb+srv://GDAP-Student:jous.trok4POOS_pood@gdap2020-cluster.swxsa.mongodb.net/<dbname>?retryWrites=true&w=majority");
        var db = client.GetDatabase("GDAP_Exercise");
        var coll = db.GetCollection<Highscore>("Highscores");
        coll.ReplaceOne(u => u.id == highscore.id, highscore);
    }
}

